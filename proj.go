package proj4m

import (
	"strings"
)

const (
	keyProjName  string = "proj"
	keyDatumCode string = "datum"
)

const (
	projCodeLatLong string = "longlat"
	projCodeMerc    string = "merc"
	projCodeBaidu   string = "bmerc"
)

const (
	datumCodeWGS84 string = "wgs84"
	datumCodeGCJ02 string = "gcj02"
	datumCodeBD09  string = "bd09"
)

type proj struct {
	projName  string
	datumCode string
	def       string
}

func newProj(def string) *proj {
	p := &proj{def: def}

	parts := strings.Split(def, "+")
	for i := range parts {
		part := strings.TrimSpace(parts[i])
		pair := strings.Split(part, "=")
		if len(pair) != 2 {
			continue
		}
		k, v := pair[0], pair[1]
		if k == keyProjName {
			p.projName = toLower(v)
			if isLatLong(p.projName) {
				p.projName = projCodeLatLong
			}
		} else if k == keyDatumCode {
			p.datumCode = toLower(v)
		}
	}

	fixMarsProj(p)

	return p
}

func fixMarsProj(p *proj) {
	if p.datumCode == datumCodeGCJ02 && !isLatLong(p.projName) {
		p.projName = projCodeMerc
	}
	if p.datumCode == datumCodeBD09 && !isLatLong(p.projName) {
		p.projName = projCodeBaidu
	}
	if p.projName == projCodeBaidu {
		p.datumCode = datumCodeBD09
	}
}

func isProjString(code string) bool {
	return strings.HasPrefix(code, "+")
}

func isLatLong(name string) bool {
	return name == "longlat" || name == "latlong" || name == "lnglat" || name == "latlng"
}
