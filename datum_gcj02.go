package proj4m

import "math"

const (
	axis   = 6378245.0
	offset = 0.00669342162296594323 //(a^2 - b^2) / a^2
)

type datumGCJ02 struct{}

func (d *datumGCJ02) toWGS84(x, y float64) (ox, oy float64) {
	ox, oy = x, y
	if outOfChina(x, y) {
		return
	}
	deltaX, deltaY := delta(x, y)
	ox -= deltaX
	oy -= deltaY
	return
}

func (d *datumGCJ02) fromWGS84(x, y float64) (ox, oy float64) {
	ox, oy = x, y
	if outOfChina(x, y) {
		return
	}
	deltaX, deltaY := delta(x, y)
	ox += deltaX
	oy += deltaY
	return
}

func transformLon(x, y float64) float64 {
	ret := 300.0 + x + 2.0*y + 0.1*x*x + 0.1*x*y + 0.1*math.Sqrt(math.Abs(x))
	ret += (20.0*math.Sin(6.0*x*math.Pi) + 20.0*math.Sin(2.0*x*math.Pi)) * 2.0 / 3.0
	ret += (20.0*math.Sin(x*math.Pi) + 40.0*math.Sin(x/3.0*math.Pi)) * 2.0 / 3.0
	ret += (150.0*math.Sin(x/12.0*math.Pi) + 300.0*math.Sin(x/30.0*math.Pi)) * 2.0 / 3.0
	return ret
}

func transformLat(x, y float64) float64 {
	ret := -100.0 + 2.0*x + 3.0*y + 0.2*y*y + 0.1*x*y + 0.2*math.Sqrt(math.Abs(x))
	ret += (20.0*math.Sin(6.0*x*math.Pi) + 20.0*math.Sin(2.0*x*math.Pi)) * 2.0 / 3.0
	ret += (20.0*math.Sin(y*math.Pi) + 40.0*math.Sin(y/3.0*math.Pi)) * 2.0 / 3.0
	ret += (160.0*math.Sin(y/12.0*math.Pi) + 320*math.Sin(y*math.Pi/30.0)) * 2.0 / 3.0
	return ret
}

func delta(wgLon, wgLat float64) (lon, lat float64) {
	dLat := transformLat(wgLon-105.0, wgLat-35.0)
	dLon := transformLon(wgLon-105.0, wgLat-35.0)
	radLat := wgLat / 180.0 * math.Pi
	magic := math.Sin(radLat)
	magic = 1 - offset*magic*magic
	sqrtMagic := math.Sqrt(magic)
	dLat = (dLat * 180.0) / ((axis * (1 - offset)) / (magic * sqrtMagic) * math.Pi)
	dLon = (dLon * 180.0) / (axis / sqrtMagic * math.Cos(radLat) * math.Pi)
	lon, lat = dLon, dLat
	return
}

func outOfChina(lon, lat float64) bool {
	if lon < 72.004 || lon > 137.8347 {
		return true
	}
	if lat < 0.8293 || lat > 55.8271 {
		return true
	}
	return false
}
