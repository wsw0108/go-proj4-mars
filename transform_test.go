package proj4m

import (
	"math"
	"testing"
)

const (
	EPSG4326 = "+proj=longlat +datum=WGS84 +no_defs"
	EPSG3857 = "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs"
)

func TestForward(t *testing.T) {
	transformer, err := NewTransformer(EPSG4326, EPSG3857)
	if err != nil {
		t.Fatal(err)
	}

	tolerance := 1e-3
	xs := []float64{120.0}
	ys := []float64{30.0}
	if err := transformer.Forward(xs, ys); err != nil {
		t.Error(err)
	}

	if math.Abs(xs[0]-13358338.895192828) > tolerance {
		t.Error(xs)
	}

	if math.Abs(ys[0]-3503549.843504374) > tolerance {
		t.Error(ys)
	}
}

func TestBackward(t *testing.T) {
	transformer, err := NewTransformer(EPSG3857, EPSG4326)
	if err != nil {
		t.Fatal(err)
	}

	tolerance := 1e-3
	xs := []float64{120.0}
	ys := []float64{30.0}
	if err := transformer.Backward(xs, ys); err != nil {
		t.Error(err)
	}

	if math.Abs(xs[0]-13358338.895192828) > tolerance {
		t.Error(xs)
	}

	if math.Abs(ys[0]-3503549.843504374) > tolerance {
		t.Error(ys)
	}
}

func TestTransformWithDef(t *testing.T) {
	transformer, err := NewTransformer("EPSG:4326", "EPSG:3857")
	if err != nil {
		t.Fatal(err)
	}

	tolerance := 1e-3
	xs := []float64{120.0}
	ys := []float64{30.0}
	if err := transformer.Forward(xs, ys); err != nil {
		t.Error(err)
	}

	if math.Abs(xs[0]-13358338.895192828) > tolerance {
		t.Error(xs)
	}

	if math.Abs(ys[0]-3503549.843504374) > tolerance {
		t.Error(ys)
	}
}

func TestTransformWithMixed(t *testing.T) {
	transformer, err := NewTransformer("EPSG:4326", EPSG3857)
	if err != nil {
		t.Fatal(err)
	}

	tolerance := 1e-3
	xs := []float64{120.0}
	ys := []float64{30.0}
	if err := transformer.Forward(xs, ys); err != nil {
		t.Error(err)
	}

	if math.Abs(xs[0]-13358338.895192828) > tolerance {
		t.Error(xs)
	}

	if math.Abs(ys[0]-3503549.843504374) > tolerance {
		t.Error(ys)
	}
}

func TestTransformSameCode(t *testing.T) {
	transformer, err := NewTransformer("GCJ02", "GCJ02")
	if err != nil {
		t.Fatal(err)
	}

	tolerance := 0.0
	xs := []float64{121.8999}
	ys := []float64{21.3333}
	if err := transformer.Forward(xs, ys); err != nil {
		t.Error(err)
	}

	if math.Abs(xs[0]-121.8999) > tolerance {
		t.Error(xs)
	}

	if math.Abs(ys[0]-21.3333) > tolerance {
		t.Error(ys)
	}
}

func TestForwardFromGCJ02ToBD09(t *testing.T) {
	transformer, err := NewTransformer("GCJ02", "BD09LL")
	if err != nil {
		t.Fatal(err)
	}

	tolerance := 1e-7
	expected := [][]float64{
		{114.69490414027017, 33.639096507711685},
		{114.69488614273101, 33.63804850387785},
		{114.69500713986416, 33.63794251496537},
		{114.69578412001135, 33.63793958798685},
		{114.6959281162725, 33.637965601694006},
		{114.69751307493384, 33.637957753486745},
	}
	coords := [][]float64{
		{114.68837663801743, 33.63312016454496},
		{114.68835840204522, 33.632072446353945},
		{114.68848002806972, 33.63196427051657},
		{114.68926112541861, 33.63194729708501},
		{114.68940588838505, 33.6319707051534},
		{114.69099925796665, 33.63193416046613},
	}
	len := len(coords)
	xs := make([]float64, len)
	ys := make([]float64, len)
	for i := range coords {
		xs[i] = coords[i][0]
		ys[i] = coords[i][1]
	}

	if err := transformer.Forward(xs, ys); err != nil {
		t.Error(err)
	}

	for i := 0; i < len; i++ {
		if math.Abs(xs[i]-expected[i][0]) > tolerance {
			t.Error(xs)
		}
		if math.Abs(ys[i]-expected[i][1]) > tolerance {
			t.Error(ys)
		}
	}
}

func TestBackwardToBD09FromGCJ02(t *testing.T) {
	transformer, err := NewTransformer("BD09LL", "GCJ02")
	if err != nil {
		t.Fatal(err)
	}

	tolerance := 1e-7
	expected := [][]float64{
		{114.69490414027017, 33.639096507711685},
		{114.69488614273101, 33.63804850387785},
		{114.69500713986416, 33.63794251496537},
		{114.69578412001135, 33.63793958798685},
		{114.6959281162725, 33.637965601694006},
		{114.69751307493384, 33.637957753486745},
	}
	coords := [][]float64{
		{114.68837663801743, 33.63312016454496},
		{114.68835840204522, 33.632072446353945},
		{114.68848002806972, 33.63196427051657},
		{114.68926112541861, 33.63194729708501},
		{114.68940588838505, 33.6319707051534},
		{114.69099925796665, 33.63193416046613},
	}
	len := len(coords)
	xs := make([]float64, len)
	ys := make([]float64, len)
	for i := range coords {
		xs[i] = coords[i][0]
		ys[i] = coords[i][1]
	}

	if err := transformer.Backward(xs, ys); err != nil {
		t.Error(err)
	}

	for i := 0; i < len; i++ {
		if math.Abs(xs[i]-expected[i][0]) > tolerance {
			t.Error(xs)
		}
		if math.Abs(ys[i]-expected[i][1]) > tolerance {
			t.Error(ys)
		}
	}
}