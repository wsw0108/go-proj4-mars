package proj4m

import "math"

const (
	r           = 6378137.0
	maxLatitude = 85.0511287798
	deg2Rad     = math.Pi / 180.0
	rad2Deg     = 180.0 / math.Pi
)

type merc struct{}

func (p *merc) Type() ProjectionType {
	return Projected
}

func (p *merc) Forward(lng, lat float64) (x, y float64) {
	lat1 := math.Max(math.Min(maxLatitude, lat), -maxLatitude)
	sin := math.Sin(lat1 * deg2Rad)
	x = r * lng * deg2Rad
	y = r * math.Log((1+sin)/(1-sin)) / 2
	return
}

func (p *merc) Inverse(x, y float64) (lng, lat float64) {
	lng = x * rad2Deg / r
	lat = (2*math.Atan(math.Exp(y/r)) - math.Pi/2) * rad2Deg
	return
}
