package proj4m

type datumWGS84 struct{}

func (d *datumWGS84) toWGS84(x, y float64) (ox, oy float64) {
	ox, oy = x, y
	return
}

func (d *datumWGS84) fromWGS84(x, y float64) (ox, oy float64) {
	ox, oy = x, y
	return
}
