package proj4m

type datum interface {
	toWGS84(x, y float64) (ox, oy float64)
	fromWGS84(x, y float64) (ox, oy float64)
}

var datumMap = map[string]datum{
	datumCodeWGS84: &datumWGS84{},
	datumCodeGCJ02: &datumGCJ02{},
	datumCodeBD09:  &datumBD09{},
}
