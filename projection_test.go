package proj4m

import (
	"math"
	"testing"
)

func TestProjectionForward(t *testing.T) {
	tolerance := 1e-7

	p1, err := NewProjection(EPSG4326)
	if err != nil {
		t.Fatal(err)
	}
	x1, y1 := 121.0, 32.0
	x1, y1 = p1.Forward(x1, y1)
	if math.Abs(x1-121.0) > tolerance {
		t.Error(x1)
	}
	if math.Abs(y1-32.0) > tolerance {
		t.Error(y1)
	}

	p2, err := NewProjection(EPSG3857)
	if err != nil {
		t.Fatal(err)
	}
	x2, y2 := 120.0, 30.0
	x2, y2 = p2.Forward(x2, y2)
	if math.Abs(x2-13358338.895192828) > tolerance {
		t.Error(x2)
	}
	if math.Abs(y2-3503549.843504374) > tolerance {
		t.Error(y2)
	}

	x3, y3 := 2.0, 1.0
	p3, err := NewProjection("+proj=tmerc   +ellps=GRS80  +lat_1=0.5 +lat_2=2 +n=0.5")
	if err != nil {
		t.Fatal(err)
	}
	x3, y3 = p3.Forward(x3, y3)
	if math.Abs(x3-222650.79679577847) > tolerance {
		t.Error(x3)
	}
	if math.Abs(y3-110642.22941192707) > tolerance {
		t.Error(y3)
	}
}

func TestProjectionInverse(t *testing.T) {
	tolerance := 1e-10

	p1, err := NewProjection(EPSG4326)
	if err != nil {
		t.Fatal(err)
	}
	x1, y1 := 121.0, 32.0
	x1, y1 = p1.Inverse(x1, y1)
	if math.Abs(x1-121.0) > tolerance {
		t.Error(x1)
	}
	if math.Abs(y1-32.0) > tolerance {
		t.Error(y1)
	}

	p2, err := NewProjection(EPSG3857)
	if err != nil {
		t.Fatal(err)
	}
	x2, y2 := 13358338.895192828, 3503549.843504374
	x2, y2 = p2.Inverse(x2, y2)
	if math.Abs(x2-120.0) > tolerance {
		t.Error(x2)
	}
	if math.Abs(y2-30.0) > tolerance {
		t.Error(y2)
	}

	x3, y3 := 200., 100.
	p3, err := NewProjection("+proj=tmerc   +ellps=GRS80  +lat_1=0.5 +lat_2=2 +n=0.5")
	if err != nil {
		t.Fatal(err)
	}
	x3, y3 = p3.Inverse(x3, y3)
	if math.Abs(x3-0.0017966305681649396) > tolerance {
		t.Error(x3)
	}
	if math.Abs(y3-0.00090436947663183841) > tolerance {
		t.Error(y3)
	}
}
