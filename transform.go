package proj4m

import (
	"errors"

	proj4 "gitlab.com/wsw0108/go-proj4"
)

type Transformer struct {
	srcCode, dstCode   string
	srcProj, dstProj   *proj
	srcProj4, dstProj4 *proj4.Proj4
	useProj4           bool
}

func NewTransformer(srcCode, dstCode string) (*Transformer, error) {
	t := &Transformer{srcCode: srcCode, dstCode: dstCode}

	srcProj, err := parseCode(srcCode)
	if err != nil {
		return nil, err
	}

	dstProj, err := parseCode(dstCode)
	if err != nil {
		return nil, err
	}

	t.srcProj = srcProj
	t.dstProj = dstProj

	useProj4 := needProj4(srcProj, dstProj)
	if useProj4 {
		srcProj4, err := proj4.NewProj4(srcProj.def)
		if err != nil {
			return nil, err
		}
		dstProj4, err := proj4.NewProj4(dstProj.def)
		if err != nil {
			return nil, err
		}
		t.srcProj4 = srcProj4
		t.dstProj4 = dstProj4
	}
	t.useProj4 = useProj4

	return t, nil
}

func (t *Transformer) Forward(xs, ys []float64) error {
	if len(xs) != len(ys) {
		return errors.New("length of xs and ys not match")
	}

	if t.srcCode == t.dstCode {
		return nil
	}
	if t.srcProj.def == t.dstProj.def {
		return nil
	}

	if t.useProj4 {
		return t.srcProj4.Transform(t.dstProj4, xs, ys)
	}

	len := len(xs)
	srcProj := projectionMap[t.srcProj.projName]
	dstProj := projectionMap[t.dstProj.projName]
	if srcProj.Type() == Projected {
		inverseMany(srcProj, xs, ys, len)
	}
	srcDatumCode := t.srcProj.datumCode
	dstDatumCode := t.dstProj.datumCode
	if srcDatumCode != dstDatumCode {
		srcDatum := datumMap[srcDatumCode]
		dstDatum := datumMap[dstDatumCode]
		if srcDatumCode == datumCodeGCJ02 && dstDatumCode == datumCodeBD09 {
			fromGCJ02Many(xs, ys, len)
		} else if srcDatumCode == datumCodeBD09 && dstDatumCode == datumCodeGCJ02 {
			toGCJ02Many(xs, ys, len)
		} else {
			toWGS84Many(srcDatum, xs, ys, len)
			fromWGS84Many(dstDatum, xs, ys, len)
		}
	}
	if dstProj.Type() == Projected {
		forwardMany(dstProj, xs, ys, len)
	}

	return nil
}

func (t *Transformer) Backward(xs, ys []float64) error {
	if len(xs) != len(ys) {
		return errors.New("length of xs and ys not match")
	}

	if t.srcCode == t.dstCode {
		return nil
	}
	if t.srcProj.def == t.dstProj.def {
		return nil
	}

	if t.useProj4 {
		return t.dstProj4.Transform(t.srcProj4, xs, ys)
	}

	len := len(xs)
	srcProj := projectionMap[t.srcProj.projName]
	dstProj := projectionMap[t.dstProj.projName]
	if dstProj.Type() == Projected {
		inverseMany(dstProj, xs, ys, len)
	}
	srcDatumCode := t.srcProj.datumCode
	dstDatumCode := t.dstProj.datumCode
	if srcDatumCode != dstDatumCode {
		srcDatum := datumMap[srcDatumCode]
		dstDatum := datumMap[dstDatumCode]
		if dstDatumCode == datumCodeGCJ02 && srcDatumCode == datumCodeBD09 {
			fromGCJ02Many(xs, ys, len)
		} else if dstDatumCode == datumCodeBD09 && srcDatumCode == datumCodeGCJ02 {
			toGCJ02Many(xs, ys, len)
		} else {
			toWGS84Many(dstDatum, xs, ys, len)
			fromWGS84Many(srcDatum, xs, ys, len)
		}
	}
	if srcProj.Type() == Projected {
		forwardMany(srcProj, xs, ys, len)
	}

	return nil
}

func (t *Transformer) ForwardPoint(x, y *float64) error {
	if t.srcCode == t.dstCode {
		return nil
	}
	if t.srcProj.def == t.dstProj.def {
		return nil
	}

	if t.useProj4 {
		return t.srcProj4.TransformPoint(t.dstProj4, x, y)
	}

	px, py := *x, *y

	srcProj := projectionMap[t.srcProj.projName]
	dstProj := projectionMap[t.dstProj.projName]
	if srcProj.Type() == Projected {
		px, py = srcProj.Inverse(px, py)
	}
	srcDatumCode := t.srcProj.datumCode
	dstDatumCode := t.dstProj.datumCode
	if srcDatumCode != dstDatumCode {
		srcDatum := datumMap[srcDatumCode]
		dstDatum := datumMap[dstDatumCode]
		if srcDatumCode == datumCodeGCJ02 && dstDatumCode == datumCodeBD09 {
			px, py = fromGCJ02(px, py)
		} else if srcDatumCode == datumCodeBD09 && dstDatumCode == datumCodeGCJ02 {
			px, py = toGCJ02(px, py)
		} else {
			px, py = srcDatum.toWGS84(px, py)
			px, py = dstDatum.fromWGS84(px, py)
		}
	}
	if dstProj.Type() == Projected {
		px, py = dstProj.Forward(px, py)
	}

	*x, *y = px, py

	return nil
}

func (t *Transformer) BackwardPoint(x, y *float64) error {
	if t.srcCode == t.dstCode {
		return nil
	}
	if t.srcProj.def == t.dstProj.def {
		return nil
	}

	if t.useProj4 {
		return t.dstProj4.TransformPoint(t.srcProj4, x, y)
	}

	px, py := *x, *y

	srcProj := projectionMap[t.srcProj.projName]
	dstProj := projectionMap[t.dstProj.projName]
	if dstProj.Type() == Projected {
		px, py = dstProj.Inverse(px, py)
	}
	srcDatumCode := t.srcProj.datumCode
	dstDatumCode := t.dstProj.datumCode
	if srcDatumCode != dstDatumCode {
		srcDatum := datumMap[srcDatumCode]
		dstDatum := datumMap[dstDatumCode]
		if dstDatumCode == datumCodeGCJ02 && srcDatumCode == datumCodeBD09 {
			px, py = fromGCJ02(px, py)
		} else if dstDatumCode == datumCodeBD09 && srcDatumCode == datumCodeGCJ02 {
			px, py = toGCJ02(px, py)
		} else {
			px, py = dstDatum.toWGS84(px, py)
			px, py = srcDatum.fromWGS84(px, py)
		}
	}
	if srcProj.Type() == Projected {
		px, py = srcProj.Forward(px, py)
	}

	*x, *y = px, py

	return nil
}

func forwardMany(p Projection, xs, ys []float64, len int) {
	for i := 0; i < len; i++ {
		xs[i], ys[i] = p.Forward(xs[i], ys[i])
	}
}

func inverseMany(p Projection, xs, ys []float64, len int) {
	for i := 0; i < len; i++ {
		xs[i], ys[i] = p.Inverse(xs[i], ys[i])
	}
}

func toWGS84Many(d datum, xs, ys []float64, len int) {
	for i := 0; i < len; i++ {
		xs[i], ys[i] = d.toWGS84(xs[i], ys[i])
	}
}

func fromWGS84Many(d datum, xs, ys []float64, len int) {
	for i := 0; i < len; i++ {
		xs[i], ys[i] = d.fromWGS84(xs[i], ys[i])
	}
}

func isMarsDatum(datum string) bool {
	return datum == datumCodeGCJ02 || datum == datumCodeBD09
}

func needProj4(srcProj, dstProj *proj) bool {
	return !isMarsDatum(srcProj.datumCode) && !isMarsDatum(dstProj.datumCode)
}
