package proj4m

import proj4 "gitlab.com/wsw0108/go-proj4"

type proj4Wrapper struct {
	proj *proj4.Proj4
}

func newProj4Wrapper(code string) (*proj4Wrapper, error) {
	proj, err := proj4.NewProj4(code)
	return &proj4Wrapper{proj: proj}, err
}

func (p *proj4Wrapper) Type() ProjectionType {
	if p.proj.IsLatLong() {
		return Geographic
	}
	return Projected
}

func (p *proj4Wrapper) Forward(lng, lat float64) (x, y float64) {
	x, y, _ = p.proj.Forward(lng, lat)
	return
}

func (p *proj4Wrapper) Inverse(x, y float64) (lng, lat float64) {
	lng, lat, _ = p.proj.Inverse(x, y)
	return
}
