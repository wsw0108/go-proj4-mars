package proj4m

import "fmt"

func parseCode(code string) (*proj, error) {
	if isAlias(code) {
		return lookupAlias(code), nil
	}

	if isProjString(code) {
		return newProj(code), nil
	}

	return nil, fmt.Errorf("Unknown code: %s", code)
}
