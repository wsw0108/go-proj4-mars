package proj4m

import "testing"

func TestRegister(t *testing.T) {
	Register("mars", "+proj=longlat +datum=GCJ02")

	if !isAlias("mars") || !isAlias("MARS") {
		t.Error("register failed")
	}
}
