package proj4m

import "strings"

func toLower(name string) string {
	return strings.ToLower(name)
}
