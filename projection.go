package proj4m

type ProjectionType int

const (
	Geographic ProjectionType = iota
	Geocentric
	Projected
)

type Projection interface {
	Forward(lng, lat float64) (x, y float64)
	Inverse(x, y float64) (lng, lat float64)
	Type() ProjectionType
}

var projectionMap = map[string]Projection{
	projCodeLatLong: &longlat{},
	projCodeMerc:    &merc{},
	projCodeBaidu:   &bmerc{},
}

func NewProjection(code string) (Projection, error) {
	proj, err := parseCode(code)
	if err != nil {
		return nil, err
	}

	if projectionMap[proj.projName] != nil {
		return projectionMap[proj.projName], nil
	}

	return newProj4Wrapper(code)
}
