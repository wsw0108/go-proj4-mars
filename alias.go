package proj4m

import "sync"

var mu sync.Mutex
var registry = make(map[string]*proj)

func init() {
	Register("EPSG:4326", "+proj=longlat +datum=WGS84 +no_defs")
	Register("WGS84", "+proj=longlat +datum=WGS84 +no_defs")
	Register("EPSG:3857", "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs")
	Register("GCJ02", "+proj=longlat +datum=GCJ02")
	Register("GCJ02MC", "+proj=merc +datum=GCJ02")
	Register("BD09LL", "+proj=longlat +datum=BD09")
	Register("BD09MC", "+proj=bmerc +datum=BD09")
}

// Register create alias for `code` with name `name`
func Register(name, code string) {
	key := toLower(name)
	mu.Lock()
	defer mu.Unlock()
	if registry[key] != nil {
		return
	}
	if isProjString(code) {
		registry[key] = newProj(code)
	}
}

func isAlias(name string) bool {
	key := toLower(name)
	mu.Lock()
	defer mu.Unlock()
	return registry[key] != nil
}

func lookupAlias(name string) *proj {
	key := toLower(name)
	mu.Lock()
	defer mu.Unlock()
	return registry[key]
}
