package proj4m

type longlat struct{}

func (p *longlat) Type() ProjectionType {
	return Geographic
}

func (p *longlat) Forward(lng, lat float64) (x, y float64) {
	x, y = lng, lat
	return
}

func (p *longlat) Inverse(x, y float64) (lng, lat float64) {
	lng, lat = x, y
	return
}
