package proj4m

import "math"

const (
	xPi = math.Pi * 3000.0 / 180.0
)

var gcj02 *datumGCJ02

func init() {
	gcj02 = &datumGCJ02{}
}

type datumBD09 struct{}

func (d *datumBD09) toWGS84(x, y float64) (ox, oy float64) {
	ox, oy = toGCJ02(x, y)
	ox, oy = gcj02.toWGS84(ox, oy)
	return
}

func (d *datumBD09) fromWGS84(x, y float64) (ox, oy float64) {
	ox, oy = gcj02.fromWGS84(x, y)
	ox, oy = fromGCJ02(x, y)
	return
}

func toGCJ02(x, y float64) (ox, oy float64) {
	x -= 0.0065
	y -= 0.006
	z := math.Sqrt(x*x+y*y) - 0.00002*math.Sin(y*xPi)
	theta := math.Atan2(y, x) - 0.000003*math.Cos(x*xPi)
	x = z * math.Cos(theta)
	y = z * math.Sin(theta)
	ox, oy = x, y
	return
}

func fromGCJ02(x, y float64) (ox, oy float64) {
	z := math.Sqrt(x*x+y*y) + 0.00002*math.Sin(y*xPi)
	theta := math.Atan2(y, x) + 0.000003*math.Cos(x*xPi)
	x = z*math.Cos(theta) + 0.0065
	y = z*math.Sin(theta) + 0.006
	ox, oy = x, y
	return
}

func toGCJ02Many(xs, ys []float64, len int) {
	for i := 0; i < len; i++ {
		xs[i], ys[i] = toGCJ02(xs[i], ys[i])
	}
}

func fromGCJ02Many(xs, ys []float64, len int) {
	for i := 0; i < len; i++ {
		xs[i], ys[i] = fromGCJ02(xs[i], ys[i])
	}
}
